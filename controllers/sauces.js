const app = require('../app');
const Sauce = require('../models/sauces');
// importation du package fs pour que les fichiers images soient bien supprimés 
const fs = require('fs');

exports.createSauce = (req, res, next) => {
  const sauceObject = JSON.parse(req.body.sauce);
  delete sauceObject._id;
  const sauce = new Sauce({
    ...sauceObject,
    imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
  });
  // méthode save() enregistre dans la bdd. Renvoie une promesse. Then : réponse de réussite. Catch : message erreur.
  sauce.save()
  .then(() => res.status(201).json({ message: 'Sauce enregistrée !' }))
  .catch(error => res.status(400).json({ error }));
};

exports.getOneSauce = (req, res, next) => {
  Sauce.findOne({ _id: req.params.id })
  .then(
    (sauce) => {
      res.status(200).json(sauce);
    }
    ).catch(
      (error) => {
        res.status(404).json({
          error: error
        });
      }
      );
    };
    
    exports.modifySauce = (req, res, next) => {
      // gestion de deux cas de figure : image modifiée ou non. Si oui, doit recvoir élément form-data + fichier. Si non, uniquement données JSON.
      const sauceObject = req.file ?
      {
        ...JSON.parse(req.body.sauce),
        imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
      } : { ...req.body };
      Sauce.updateOne({ _id: req.params.id }, { ...sauceObject, _id: req.params.id })
      .then(() => res.status(200).json({ message: 'Sauce modifiée !' }))
      .catch(error => res.status(400).json({ error }));
    };
    
    exports.likeSauce = (req, res, next) => {
      Sauce.findOne({ _id: req.params.id })
      .then((sauce) => {
        
        const likeValue = req.body.like;
        console.log(likeValue);
        var idUser = req.body.userId;
        
        // Check if the user already liked the sauce      
        var tableOfTheUsersLiked = sauce.usersLiked;
        var alreadyLiked = tableOfTheUsersLiked.includes(idUser);
        // return true if included
        
        // Check if the user already disliked the sauce
        var tableOfTheUsersDisliked = sauce.usersDisliked;
        var alreadyDisliked = tableOfTheUsersDisliked.includes(idUser);
        // return true if included
        
        if (likeValue === 1) {
          if (!alreadyLiked) {
            Sauce.updateOne({ _id: req.params.id }, { $inc: { likes: 1 }, $push: { usersLiked: req.body.userId } })
            .then(() => res.status(200).json({ message: 'Sauce aimée !' }))
            .catch(error => res.status(400).json({ error }));
          } else {
            console.log("Sauce déjà aimée");
          }
        }
        else if (likeValue === -1) {
          if (!alreadyDisliked) {
            Sauce.updateOne({ _id: req.params.id }, { $inc: { dislikes: 1 }, $push: { usersDisliked: req.body.userId } })
            .then(() => res.status(200).json({ message: 'Sauce non aimée !' }))
            .catch(error => res.status(400).json({ error }));
          } else {
            console.log("Sauce déjà non aimée");
          }        
        } 
        else {
          if (alreadyLiked) {
            Sauce.updateOne({ _id: req.params.id }, { $inc: { likes: -1 }, $pull: { usersLiked: req.body.userId } })
            .then(() => res.status(200).json({ message: 'Like annulé !' }))
            .catch(error => res.status(400).json({ error }));
          }
          if (alreadyDisliked) {
            Sauce.updateOne({ _id: req.params.id }, { $inc: { dislikes: -1 }, $pull: { usersDisliked: req.body.userId } })
            .then(() => res.status(200).json({ message: 'Dislike annulé !' }))
            .catch(error => res.status(400).json({ error }));
          }
        } 
      })
      .catch(error => res.status(500).json({ error }));
    };
    
    exports.deleteSauce = (req, res, next) => {
      Sauce.findOne({ _id: req.params.id })
      .then(sauce => {
        const filename = sauce.imageUrl.split('/images/')[1];
        fs.unlink(`images/${filename}`, () => {
          Sauce.deleteOne({ _id: req.params.id })
          .then(() => res.status(200).json({ message: 'Sauce supprimée !' }))
          .catch(error => res.status(400).json({ error }));
        });
      })
      .catch(error => res.status(500).json({ error }));
    };
    
    exports.getAllSauces = (req, res, next) => {
      // méthode find() afin de renvoyer un tableau contenant toutes les sauces dans notre bdd
      Sauce.find().then(
        // bdd mongoose organisée en collections dont nom par défaut est le nom du modèle au pluriel (donc sauces)
        (sauces) => {
          res.status(200).json(sauces);
        }
        ).catch(
          (error) => {
            res.status(400).json({
              error: error
            });
          }
          );
        };
        
