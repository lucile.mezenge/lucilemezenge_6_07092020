// importe package de chiffrement bcrypt afin haut niveau de sécurisation sur mdp
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const User = require('../models/user');
const env = require('../env');


exports.signup = (req, res, next) => {
  // appelle fonction de hachage de bcrypt avec valeur de 10 pour "saler" le mdp
    bcrypt.hash(req.body.password, 10)
        .then(hash => {
            const user = new User({
                email: req.body.email,
                password: hash
            });
            user.save()
                .then(() => res.status(201).json({ message: 'Utilisateur créé !' }))
                .catch(error => res.status(400).json({ error }));
        })
        .catch(error => res.status(500).json({ error }));
};

exports.login = (req, res, next) => {
    User.findOne({ email: req.body.email })
      .then(user => {
        if (!user) {
          return res.status(401).json({ error: 'Utilisateur non trouvé !' });
        }
        // utilise méthode compare de bcrypt pour comparer mdp
        bcrypt.compare(req.body.password, user.password)
          .then(valid => {
            if (!valid) {
              return res.status(401).json({ error: 'Mot de passe incorrect !' });
            }
            res.status(200).json({
              userId: user._id,
              // fonction sign dejsonwebtoken pour encoder un nouveau token              
              token: jwt.sign(
                // ce token contient l'ID de l'utilisateur (encodé dans le token)
                { userId: user._id },
                // nous utilisons une chaîne secrète de développement temporaire RANDOM_SECRET_KEY pour encoder notre token (à remplacer par une chaîne aléatoire beaucoup plus longue pour la production)
                env.varToken,
                // durée de validité du token à 24 heures. L'utilisateur devra donc se reconnecter au bout de 24 heures
                { expiresIn: '24h' }
              )
            });
          })
          .catch(error => res.status(500).json({ error }));
      })
      .catch(error => res.status(500).json({ error }));
  };

