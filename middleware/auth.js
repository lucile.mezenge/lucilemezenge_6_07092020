const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    // étant donné que de nombreux problèmes peuvent se produire, nous insérons tout à l'intérieur d'un bloc try...catch
  try {
    // extrait le token du header Authorization de la requête entrante. Utilise la fonction split pour récupérer tout après l'espace dans le header
    const token = req.headers.authorization.split(' ')[1];
    // utilise la fonction verify pour décoder notre token. Si celui-ci n'est pas valide, une erreur sera générée
    const decodedToken = jwt.verify(token, 'RANDOM_TOKEN_SECRET');
    // extrait l'ID utilisateur de notre token
    const userId = decodedToken.userId;
    // si la demande contient un ID utilisateur, nous le comparons à celui extrait du token. S'ils sont différents, nous générons une erreur
    if (req.body.userId && req.body.userId !== userId) {
      throw 'Invalid user ID';
    } else {
      next();
    }
  } catch {
    res.status(401).json({
      error: new Error('Invalid request!')
    });
  }
};