const multer = require('multer');

const MIME_TYPES = {
  'image/jpg': 'jpg',
  'image/jpeg': 'jpg',
  'image/png': 'png'
};

const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    // fonction destination indique d'enregistrer les fichiers dans le dossier images
    callback(null, 'images');
  },
  filename: (req, file, callback) => {
    // fonction filename indique de renommer avec nom d'origine + underscores + timestamp + extension appropriée
    const name = file.originalname.split(' ').join('_');
    const extension = MIME_TYPES[file.mimetype];
    callback(null, name + Date.now() + '.' + extension);
  }
});

module.exports = multer({storage }).single('image');