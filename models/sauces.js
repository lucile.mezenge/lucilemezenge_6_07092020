const mongoose = require('mongoose');

// Création schéma de données qui correspond aux champs souhaités. Utilise méthode Schéma de mongoose.

const sauceSchema = mongoose.Schema({
    name: { type: String, required:true },
    manufacturer: { type: String, required:true },
    description: { type: String, required:true },
    mainPepper: { type: String, required:true },
    imageUrl: { type: String, required:true },
    heat: { type: Number, required: true },
    likes: { type: Number, default: 0 },
    dislikes: { type: Number, default: 0 },
    usersLiked: [ { type: String } ],
    usersDisliked: [ { type: String } ],
    userId: { type: String, required: true }
  });

  module.exports = mongoose.model('Sauce', sauceSchema);

